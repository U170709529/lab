public class GCDLoop {
	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		;
		int smaller = a > b ? b : a;
		int greater = a < b ? b : a;
		int gcdResult = gcd(smaller, greater);
		System.out.println("the GCD for :" +greater + "+" + smaller + " = " + gcdResult);


	}

	private static int gcd(int a, int b) {
		int remainder = a % b;
		while (remainder != 0) {
			a = b;
			b = remainder;
			remainder = a % remainder;
		}
		return b;
	}
}

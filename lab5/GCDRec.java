public class GCDRec {
	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);

		int smaller = a > b ? b : a;
		int greater = a < b ? b : a;
		int gcdResult = gcd(smaller, greater);
		System.out.println("the GCD for :" + greater + "+" + smaller + " = " + gcdResult);

	}

	private static int gcd(int greater, int smaller) {
		if (smaller == 0)
			return greater;
		return gcd(smaller, greater % smaller);
	}
}

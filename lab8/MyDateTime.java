public class MyDateTime {
	MyDate myDate;
	MyTime myTime;

	MyDateTime(MyDate date, MyTime time) {
		myDate = date;
		myTime = time;
	}

	public void incrementDay() {
		myDate.incrementDay();
	}

	public void incrementHour(int i) {
		incrementMinute(i * 60);
	}

	public void decrementHour(int i) {
		decrementMinute(i * 60);
	}

	public void incrementMinute(int i) {
		int days = (myTime.mins + i) / 1440;
		if (days > 0)
			myDate.incrementDay(days);
		myTime.incrementMinute(i);
	}

	public void decrementMinute(int i) {
		int days = (myTime.mins - i) / 1440;
		int extra = (myTime.mins - i) % 1440;
		if (days < 0)
			days = -days;
		if (extra < 0)
			days++;
		if (days > 0)
			myDate.decrementDay(days);
		myTime.decrementMinute(i);
	}

	public void incrementYear(int i) {
		myDate.incrementYear(i);
	}

	public void decrementDay() {
		myDate.decrementDay();
	}

	public void decrementYear() {
		myDate.decrementYear();
	}

	public void decrementMonth() {
		myDate.decrementMonth();
	}

	public void incrementDay(int i) {
		myDate.incrementDay(i);
	}

	public void decrementMonth(int i) {
		myDate.decrementMonth(i);
	}

	public void decrementDay(int i) {
		myDate.decrementDay(i);
	}

	public void incrementMonth(int i) {
		myDate.incrementMonth(i);
	}

	public void decrementYear(int i) {
		myDate.decrementYear(i);
	}

	public void incrementMonth() {
		myDate.incrementMonth();
	}

	public void incrementYear() {
		myDate.incrementYear();
	}

	public boolean isBefore(MyDateTime anotherDateTime) {
		boolean isBefore = myDate.isBefore(anotherDateTime.myDate);
		return isBefore || (myTime.mins < anotherDateTime.myTime.mins);
	}

	public boolean isAfter(MyDateTime anotherDateTime) {
		boolean isAfter = myDate.isAfter(anotherDateTime.myDate);
		return isAfter || (myTime.mins > anotherDateTime.myTime.mins);
	}

	public String dayTimeDifference(MyDateTime anotherDateTime) {
        /*
        int leapYearsBeforeA=(myDate.year / 4) - (myDate.year / 100) + (myDate.year / 400);
        int normalYearsA=myDate.year-leapYearsBeforeA;
        int daysA=(normalYearsA*365)+(leapYearsBeforeA * 366) +(myDate.maxDays[myDate.month]) +myDate.day;
        int minsA=(daysA*1440)+myTime.mins;


        int leapYearsBeforeB=(anotherDateTime.myDate.year / 4) - (anotherDateTime.myDate.year / 100) + (anotherDateTime.myDate.year / 400);
        int normalYearsB=anotherDateTime.myDate.year-leapYearsBeforeA;
        int daysB=(normalYearsB*365)+(leapYearsBeforeB * 366)+(anotherDateTime.myDate.maxDays[anotherDateTime.myDate.month]) + anotherDateTime.myDate.day;
        int minsB=(daysB*1440)+anotherDateTime.myTime.mins;

        int differenceInMins=minsA-minsB;
        if(differenceInMins<0)
            differenceInMins=-differenceInMins;
        */
		int days = myDate.dayDifference(anotherDateTime.myDate);

		int mins = myTime.minsDifference(anotherDateTime.myTime);
		if (anotherDateTime.myTime.mins < myTime.mins) {
			days--;
			mins = 1440 - mins;
		}

		int hours = mins / 60;
		int realMins = mins % 60;
		return (days > 0 ? days + " day(s) " : "") + (hours > 0 ? hours + " hour(s) " : "") + (realMins > 0 ? realMins + " minute(s)" : "");
	}

	public void incrementHour() {
		myTime.incrementHour();
	}

	public String toString() {
		return myDate.toString() + " " + myTime.toString();
	}
}


public class MyTime {
	int mins; // 1440

	MyTime(int hour, int min) {
		this.mins = (hour * 60) + min;
	}

	public void incrementHour(int i) {
		incrementMinute(i * 60);
	}

	public void decrementHour(int i) {
		decrementMinute(i * 60);
	}

	public void incrementMinute(int i) {
		mins += i;
		int extra = mins % 1440;
		mins = extra;
	}

	public void decrementMinute(int i) {
		mins -= i;
		int extra = mins % 1440;
		mins = (extra < 0 ? 1440 + extra : extra);
	}

	public void incrementHour() {
		incrementHour(1);
	}

	public String toString() {
		return (mins / 60 < 10 ? "0" : "") + (mins / 60) + ":" + (mins % 60 < 10 ? "0" : "") + (mins % 60);
	}

	public int minsDifference(MyTime anotherMyTime) {
		int dif = mins - anotherMyTime.mins;
		if (dif < 0)
			dif = -dif;
		return dif;
	}
}

package shapes2d;

public class Circle /*extends Object*/ {

	protected double radius;

	public Circle() {
		radius = 1.0;
	}


	public void setRadius(double d) {
		this.radius = d;
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public double area() {
		return Math.PI * radius * radius;
	}
	public double getRadius() {
		return radius;
	}

	@Override
	public String toString() {
		return "Circle{" +
			"radius=" + radius +
			'}';
	}

}

package shapes2d;

public class Square {

	protected double side;

	public Square() {
		side = 1.0;
	}


	public Square(double side) {
		this.side = side;
	}

	public double area() {
		return side * side;
	}


	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}


	@Override
	public String toString() {
		return "Square{" +
			"side=" + side +
			'}';
	}
}

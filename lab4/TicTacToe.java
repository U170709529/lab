import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

		printBoard(board);

		int moveCount = 0;
		int currentPlayer = 0;
		boolean win = false;
		while (moveCount < 9) {
			System.out.print("Player" + (currentPlayer + 1) + "enter row num:");
			int row = reader.nextInt();
			System.out.print("Player " + (currentPlayer + 1) + "enter col num:");
			int col = reader.nextInt();
			if (row > 0 && row < 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' ') {

				if (currentPlayer == 0)
					board[row - 1][col - 1] = 'X';
				else board[row - 1][col - 1] = 'O';

				printBoard(board);
				win = checkBoard(board);
				if (win) {
					System.out.print("Player" + (currentPlayer + 1) + "Won");
					break;
				}

				moveCount++;
				currentPlayer = (currentPlayer + 1) % 2;
			} else {
				System.out.println("the move not valid");
			}

		}
		if (moveCount == 9)
			System.out.println("the game ended with a drow");

		reader.close();
	}

	public static boolean checkBoard(char[][] board) {
		for (int row = 0; row < 3; row++) {
			if ((board[row][0] == board[row][1]) && (board[row][1] == board[row][2]) && ((board[row][0]) != ' '))
				return true;
		}

		for (int col = 0; col < 3; col++) {
			if ((board[col][0] == board[col][1]) && (board[col][1] == board[col][2]) && ((board[col][0]) != ' '))
				return true;
		}

		if ((board[0][0] == board[1][1]) && (board[1][1] == board[2][2]) && ((board[0][0]) != ' '))
			return true;

		if ((board[2][0] == board[1][1]) && (board[1][1] == board[0][2]) && ((board[2][0]) != ' '))
			return true;
		return false;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}

class Circle {
	int radius;
	Point center;

	public Circle(int radius, Point center) {
		this.radius = radius;
		this.center = center;
	}

	public double area() {
		return Math.PI * Math.pow(radius, 2);
	}

	public double perimeter() {
		return (int) Math.round(2 * Math.PI * radius);
	}

	public boolean intersect(Circle circle) {
		return ((radius + circle.radius) >= center.distanceFromAnotherPoint(circle.center));
	}
}

class Rectangle {
	private int sideA, sideB;
	private Point topLeft;

	Rectangle(int sideA, int sideB, Point topLeft) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.topLeft = topLeft;
	}

	int area() {
		return sideA * sideB;
	}

	int perimeter() {
		return (sideA * 2) + (sideB * 2);
	}

	public Point[] corners() {
		Point[] corners = new Point[4];
		corners[0] = topLeft;
		corners[1] = new Point(topLeft.xCoord + sideA, topLeft.yCoord);
		corners[2] = new Point(topLeft.xCoord, topLeft.yCoord - sideB);
		corners[3] = new Point(topLeft.xCoord + sideA, topLeft.yCoord - sideB);
		return corners;
	}
}

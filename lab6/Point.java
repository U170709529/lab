class Point {
	int xCoord, yCoord;

	Point(int xCoord, int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}

	public double distanceFromAnotherPoint(Point p) {
		int xdiff = xCoord - p.xCoord;
		int ydiff = yCoord - p.yCoord;
		return Math.sqrt(Math.pow(xdiff, 2) + Math.pow(ydiff, 2));

	}
}

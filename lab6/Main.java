public class Main {
	public static void main(String[] args) {
		Point tl1 = new Point(5, 7);
		System.out.println("tl1 is located on the " + tl1.xCoord + tl1.yCoord);

		Rectangle rect1 = new Rectangle(10, 5, tl1);
		System.out.println("Area: " + rect1.area() + "\nPerimeter: " + rect1.perimeter());

		Point[] rect1Corners = rect1.corners();

		for (int i = 0; i < rect1Corners.length; i++) {
			System.out.println("The " + (i + 1) + "corners located at " + rect1Corners[i].xCoord + ":" + rect1Corners[i].yCoord);
		}

		//--
		Circle circle1 = new Circle(10, new Point(0, 0));
		System.out.println("Area: " + circle1.area() + "the permiter =" + circle1.perimeter());
		Circle c2 = new Circle(5, new Point(1, 5));
		boolean isIntersect = circle1.intersect(circle1);
		if (isIntersect) {
			System.out.println("c1 and c2 are intersecting");
		} else System.out.println("c1 and c2 are not intersecting");

	}
}

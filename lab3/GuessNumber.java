import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class

		int attempts=0;
		System.out.print("Hi! I'm thinking of a number between 0 and 99. \nCan you guess it: ");
		int number =rand.nextInt(100); //generates a number between 0 and 99
		while(true){
			attempts++;
			int guess = reader.nextInt(); //Read the user input
			if(guess==-1){
				System.out.println("Sorry, the number was "+number+".");
				break;
			}
			else if (guess==number){
				System.out.println("Congratulations! You won after "+attempts+ (attempts>1? " attempts!" : " attempt!"));
				break;
			}
			else{
				if(number < guess)
					System.out.println("Mine is less than your guess.");
				else
					System.out.println("Mine is greater than your guess.");
				System.out.print("Type -1 to quit or guess another: ");
			}
		}




		reader.close(); //Close the resource before exiting
	}


}

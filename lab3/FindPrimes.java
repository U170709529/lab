public class FindPrimes {
	public static void main(String[] args) {
		int max = Integer.parseInt(args[0]);

		for (int num = 3; num <= max; num++) {
			boolean isPrime = true;
			for (int divisor = 2; divisor < num; divisor++) {
				if (num % divisor == 0) {
					isPrime = false;
				}
			}
			if (isPrime) {
				System.out.print(num+" ,");
			}
		}
	}
}

public class MyDate {
	private int day, month, year;

	MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	private boolean isLeapYear(int year) {
		if (year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0)
					return true;
				else
					return false;
			} else
				return true;
		} else
			return false;
	}

	int[] monthDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	private int daysInMonth(int month) {
		if (month == 2 && isLeapYear(year))
			return monthDays[--month] + 1;
		return monthDays[--month];
	}

	public void incrementDay(int i) {
		for (; i > 0; i--)
			incrementDay();
	}

	public void incrementDay() {
		int fullDays = daysInMonth(month);
		if (day < fullDays)
			day++;
		else {
			incrementMonth();
			day = 1;
		}
	}

	public void incrementMonth(int i) {
		int years = i / 12;
		incrementYear(years);
		month += i - (12 * years);
		if(month>12){
			month=month-12;
			incrementYear();
		}
		fixMonths();
	}

	public void incrementMonth() {
		incrementMonth(1);
	}

	public void incrementYear(int i) {
		year += i;
		fixMonths();
	}

	public void incrementYear() {
		incrementYear(1);
	}

	public void decrementDay(int i) {
		for (; i > 0; i--)
			decrementDay();
	}

	public void decrementDay() {
		if (day > 1)
			day--;
		else {
			decrementMonth();
			day = daysInMonth(month);
		}
	}

	public void decrementMonth(int i) {
		int years = i / 12;
		decrementYear(years);
		month -= i - (12 * years);
		if(month<1){
			month=12+month;
			decrementYear();
		}
		fixMonths();
	}

	public void decrementMonth() {
		decrementMonth(1);
	}

	public void decrementYear(int i) {
		year -= i;
		fixMonths();
	}

	public void decrementYear() {
		decrementYear(1);
	}

	public boolean isAfter(MyDate anotherDate) {
		if (year > anotherDate.year)
			return true;
		if (year == anotherDate.year && month > anotherDate.month)
			return true;
		if (year == anotherDate.year && month == anotherDate.month && day > anotherDate.day)
			return true;
		return false;
	}

	public boolean isBefore(MyDate anotherDate) {
		if (year < anotherDate.year)
			return true;
		if (year == anotherDate.year && month < anotherDate.month)
			return true;
		if (year == anotherDate.year && month == anotherDate.month && day < anotherDate.day)
			return true;
		return false;
	}

	public int dayDifference(MyDate anotherDate) {
		int days = 0;
		while (isAfter(anotherDate)) {
			decrementDay();
			days++;
		}
		while (isBefore(anotherDate)) {
			incrementDay();
			days++;
		}
		return days;
	}

	private void fixMonths() {
		int days = daysInMonth(month);
		if (day > days)
			day = days;

	}

	public String toString() {
		return year + "-" + (month < 10 ? "0" : "") + month + "-" + (day < 10 ? "0" : "") + day;
	}
}
